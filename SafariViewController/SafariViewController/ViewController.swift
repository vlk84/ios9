//
//  ViewController.swift
//  SafariViewController
//
//  Created by Vladimir Kolbun on 12.06.15.
//  Copyright © 2015 Vladimir Kolbun. All rights reserved.
//

import UIKit
import SafariServices

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, SFSafariViewControllerDelegate {
  let kCellID = "CellID"
  let items =  [ActionItem("Simple URL", "http://yandex.ru"),
                ActionItem("Crashing URL", "http://computerbytesman.com/security/crashes/index.htm")]


  @IBOutlet var tableView : UITableView!

  //MARK: Controller life-time
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: kCellID)
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  //MARK: Table data source
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.items.count
  }

  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier(kCellID)!
    cell.textLabel?.text = self.items[indexPath.row].title

    return cell
  }

  //MARK: Table delegate
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if let url = NSURL(string: self.items[indexPath.row].urlString) {
      let safari = SFSafariViewController(URL: url)
      safari.delegate = self
      self.presentViewController(safari, animated: true, completion: nil)
    }
  }

  //MARK: Safari delegate
  func safariViewControllerDidFinish(controller: SFSafariViewController) {
    self.dismissViewControllerAnimated(true, completion: nil)
  }
}

