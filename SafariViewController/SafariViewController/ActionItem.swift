//
//  ActionItem.swift
//  SafariViewController
//
//  Created by Vladimir Kolbun on 12.06.15.
//  Copyright © 2015 Vladimir Kolbun. All rights reserved.
//

import Foundation

struct ActionItem {
  let title : String
  let urlString : String

  init(_ title : String, _ urlString: String) {
    self.title = title
    self.urlString = urlString
  }
}